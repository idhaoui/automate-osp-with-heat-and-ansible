require 'net/http'
begin
def get_REST(uri)
  return Net::HTTP::Get.new(uri.request_uri)
end

def post_REST(uri,body)
  header= {'Content-Type': 'application/json'}
  request = Net::HTTP::Post.new(uri.request_uri, header)
  request.body = body.to_s
  return request
end

def call_REST(url,action,message=nil)
  $evm.log(:info, "call_REST: calling #{url} with #{message}")
  uri=URI.parse(url)
  body=message.to_s
  
  response = Net::HTTP.start(uri.host, uri.port,:verify_mode => OpenSSL::SSL::VERIFY_NONE,  :use_ssl => uri.scheme == 'https') do |http|
        request =post_REST(uri,body) if action=="create"
        request = get_REST(uri) if action=="query"
      request.basic_auth('admin','r3dh4t1!')
        http.request(request)
    end
    $evm.log(:info, "call_REST: code=#{response.code}")
  $evm.log(:info, "call_REST: body=#{response.body}")
  $evm.log(:info, "call_REST: content_type=#{response.content_type}")
  return response
end

def get_tenants(endpoint)
  url="#{endpoint}/tenants"
  response=call_REST(url,"query")
  j_response=JSON.parse(response.body) if response.content_type=="application/json"
  result=j_response["resources"] rescue nil
  return result
end

def get_tenant_by_name(endpoint,name)
  tenants=get_tenants(endpoint)
  my_tenant=nil
  tenants.each do |t|
    tenant=call_REST(t["href"],"query")
    
    my_tenant=tenant.body;break if tenant.body["name"]==name
  end
  j_tenant=JSON.parse(my_tenant)
  $evm.log(:info,"get_tenant_by_name: #{my_tenant.inspect}")
  return j_tenant
end

def get_rootTenant_url(endpoint)
  tenants=get_tenants(endpoint)
  root_url=tenants.first["href"] rescue nil
    $evm.log(:info,"get_tenant_url: #{root_url.inspect}")
  return root_url
end

def get_rootTenant_id(endpoint)
  root_url=get_rootTenant_url(endpoint)
  response=call_REST(root_url,"query")
  j_response=JSON.parse(response.body) if response.content_type=="application/json"
  result=j_response["id"]
    $evm.log(:info,"get_tenant_id: #{result.inspect}")
  return result
end

def create_tenant (endpoint,name)
  parent=get_rootTenant_id(endpoint)
  template_body = '{"name":"%{name}","description":"%{name}","parent":{"id":"%{parent}"}}'
  body = template_body % {:name => "#{name}", :parent=>parent}
  
  url="#{endpoint}/tenants"
  response=call_REST(url,"create",body)
  result=response.body
  result=JSON.parse(response.body) if response.content_type=="application/json"
    $evm.log(:info,"create_tenant: #{result.inspect}")
  return result 
end

def create_group(endpoint,grp_name,role,tenant_name)

  tenant=get_tenant_by_name(endpoint,tenant_name)
  tenant_id=tenant["id"] rescue nil
  $evm.log(:info,"create_group: tenant[#{tenant_name}]=#{tenant["id"]}")

  template_body = '{"description":"%{name}","role":{"name":"%{role}"},"tenant":{"id":"%{tenant}"}}'
  body = template_body % {:name => "#{grp_name}", :role=>role, :tenant=>tenant_id}

  url="#{endpoint}/groups"
  response=call_REST(url,"create",body)
  result=response.body
  result=JSON.parse(response.body) if response.content_type=="application/json"
    $evm.log(:info,"create_group: #{result.inspect}")
  return result 
end


  endpoint="https://cfui.example.com/api"
  
  task = $evm.root["service_template_provision_task"]
  service = task.try(:destination)

  unless service
    $evm.log(:error, 'Service is nil')
    raise 'Service is nil'
  end
  
  env=service.get_dialog_option("dialog_param_environment")
  tenant_name=service.get_dialog_option("dialog_param_tenant_name")
  tenant_id=service.get_dialog_option("dialog_param_tenant_id")
  tenant_description="#{tenant_name}-#{env}-#{tenant_id}"
  
  #tenant=create_tenant(endpoint,tenant_description)
  tenant=get_tenant_by_name(endpoint,tenant_description)
  if tenant.nil?
    $evm.log(:info, 'tenant #{tenant_description} not ready, RETRYING')
    $evm.root["ae_status"]="retry"
    $evm.root["ae_retry_delay"]=60.seconds
  else
    admin_grp=create_group(endpoint,"ADMIN_TENANT_#{tenant_name}-#{env}-#{tenant_id}","EvmRole-tenant_administrator",tenant_description)
    admin_grp=create_group(endpoint,"MEMBER_TENANT_#{tenant_name}-#{env}-#{tenant_id}","EvmRole-tenant_user",tenant_description)
  end
      $evm.root['ae_result']         = "ok"
    $evm.root['ae_retry_interval'] = 20.seconds
rescue Exception => e  
    $evm.log("error", " ##################################################### ERROR RETRYING #########################")
    $evm.log("error", " err=#{exception.message}") rescue nil
    $evm.log("error", "[#{e}]\n#{e.backtrace.join("\nERROR: ")}") rescue nil  
    $evm.root['ae_result']         = "retry"
    $evm.root['ae_retry_interval'] = 20.seconds
    exit MIQ_OK
end
