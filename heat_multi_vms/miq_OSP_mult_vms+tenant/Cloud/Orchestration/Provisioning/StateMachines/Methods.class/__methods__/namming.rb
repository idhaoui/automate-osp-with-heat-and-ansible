#
# Description: This class checks to see if the stack has been provisioned
#   and whether the refresh has completed
#
module ManageIQ
  module Automate
    module Cloud
      module Orchestration
        module Provisioning
          module StateMachines
            class SetService
              def initialize(handle = $evm)
                @handle = handle
              end

              def main
                task = @handle.root["service_template_provision_task"]
                service = task.try(:destination)

                unless service
                  @handle.log(:error, 'Service is nil')
                  raise 'Service is nil'
                end
                setService(service)
              end

              private

              def setService(service)
                parent=@handle.vmdb(:service).find_by_name("organisations")
                if parent.nil?
                  parent=@handle.vmdb(:service).create(:name=>"organisations")
                  parent.display=true
                end
                service.parent_service=parent
              end
            end
          end
        end
      end
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  ManageIQ::Automate::Cloud::Orchestration::Provisioning::StateMachines::SetService.new.main
end
